﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Design.PluralizationServices;
using System.Dynamic;
using System.Globalization;
using System.Linq;
using System.Linq.Expressions;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace DynamicXml
{
    public class DynamicXml : DynamicObject
    {
        const string CultureCode = "en-US";

        XElement element;

        private DynamicXml(XElement root)
        {
            element = root;
        }

        public static dynamic Parse(string xmlString)
        {
            if (IsCollection(XDocument.Parse(xmlString).Root))
            {
                var values = new List<dynamic>();

                foreach (var item in XDocument.Parse(xmlString).Root.Elements())
                    values.Add(item.HasElements ? new DynamicXml(item) : (dynamic)item.Value);

                return values;
            }

            return new DynamicXml(XDocument.Parse(xmlString).Root);
        }

        private static bool IsCollection(XElement element)
        {            
            var pluralizationService =
                PluralizationService.CreateService(CultureInfo.CreateSpecificCulture(CultureCode));

            var result = element.HasElements && (element.Elements().Count() > 1 && element.Elements().All(e => ((element.Parent != null) && element.Parent.Name.LocalName.ToLower().Contains(e.Name.LocalName.ToLower()) && e.Name.LocalName.ToLower() == element.Elements().First().Name.LocalName.ToLower())) || element.Name.LocalName.ToLower() == pluralizationService.Pluralize(element.Elements().First().Name.LocalName.ToLower()));

            return result;
        }

        private object GetXMLMember(string name)
        {
            if (element.Name.LocalName == name)
            {
                return new DynamicXml(element);
            }

            var att = element.Attribute(name);
            if (att != null)
            {
                return att.Value;
            }

            var node = element.Element(name);
            if (node != null && !node.HasElements)
            {
                return node.Value;
            }

            var nodes = element.Elements(name);
            if (nodes.Count() > 1)
            {
                return nodes.Select(n => n.HasElements ? (dynamic)new DynamicXml(n) : n.Value).ToList();
            }

            if (node != null && node.HasElements)
            {
                if (IsCollection(node))
                {
                    var items = node.Elements().ToList();

                    var values = new List<dynamic>();

                    foreach (var item in items)
                        values.Add(item.HasElements ? new DynamicXml(item) : (dynamic)item.Value);

                    return values.AsEnumerable();
                }
                else
                {
                    return node.HasElements ? (dynamic)new DynamicXml(node) : node.Value;
                }
            }

            return null;
        }

        public override IEnumerable<string> GetDynamicMemberNames()
        {
            return base.GetDynamicMemberNames();
        }

        public override bool TryBinaryOperation(BinaryOperationBinder binder, object arg, out object result)
        {
            return base.TryBinaryOperation(binder, arg, out result);
        }

        public override bool TryConvert(ConvertBinder binder, out object result)
        {
            return base.TryConvert(binder, out result);
        }

        public override DynamicMetaObject GetMetaObject(Expression parameter)
        {
            return base.GetMetaObject(parameter);
        }

        public override bool TryCreateInstance(CreateInstanceBinder binder, object[] args, out object result)
        {
            return base.TryCreateInstance(binder, args, out result);
        }

        public override bool TryUnaryOperation(UnaryOperationBinder binder, out object result)
        {
            return base.TryUnaryOperation(binder, out result);
        }

        public override bool TryInvokeMember(InvokeMemberBinder binder, object[] args, out object result)
        {
            return TryGetXmlValue(binder.Name, out result);
        }

        public override bool TryGetMember(GetMemberBinder binder, out object result)
        {
            return TryGetXmlValue(binder.Name, out result);
        }

        private bool TryGetXmlValue(string name, out object result)
        {
            result = GetXMLMember(name);

            if (result != null)
                return true;

            var nameTitleCase = name.Substring(0, 1).ToLower() + name.Substring(1);

            result = GetXMLMember(nameTitleCase);

            if (result != null)
                return true;

            var nameCamelCase = name.Substring(0, 1).ToUpper() + name.Substring(1);

            result = GetXMLMember(nameCamelCase);

            if (result != null)
                return true;

            return false;
        }
    }
}
