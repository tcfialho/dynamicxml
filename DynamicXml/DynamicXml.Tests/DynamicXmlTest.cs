﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace DynamicXml.Tests
{
    [TestClass]
    public class DynamicXmlTest
    {
        [TestMethod]
        public void ShouldReadProperty()
        {
            // Arrange
            var xml = @"<?xml version=""1.0""?>
                        <MyObject>
                            <PropertyOne>xxx</PropertyOne>
                        </MyObject>";

            // Act
            dynamic obj = DynamicXml.Parse(xml);

            // Assert
            Assert.AreEqual("xxx", obj.PropertyOne);
        }

        [TestMethod]
        public void ShouldReadXmlAttribute()
        {
            // Arrange
            var xml = @"<?xml version=""1.0""?>
                        <MyObject AttrOne = ""123"">
                        </MyObject>";

            // Act
            dynamic obj = DynamicXml.Parse(xml);

            // Assert
            Assert.AreEqual("123", obj.AttrOne);
        }

        [TestMethod]
        public void ShouldReadMultipleNodes()
        {
            // Arrange
            var xml = @"<?xml version=""1.0""?>
                        <MyObject>
                            <Tables>
                                <Table>
                                    <Name>eee</Name>
                                </Table>
                                <Table>
                                    <Name>fff</Name>
                                </Table>
                            </Tables>
                        </MyObject>";

            // Act
            dynamic obj = DynamicXml.Parse(xml);

            // Assert
            Assert.AreEqual(2, obj.Tables.Count);
            Assert.AreEqual("eee", obj.Tables[0].Name);
            Assert.AreEqual("fff", obj.Tables[1].Name);
        }

        [TestMethod]
        public void ShouldReadSingleNode()
        {
            // Arrange
            var xml = @"<?xml version=""1.0""?>
                        <MyObject>
                            <Table>
                                <Name>eee</Name>
                            </Table>
                        </MyObject>";

            // Act
            dynamic obj = DynamicXml.Parse(xml);

            // Assert
            Assert.AreEqual("eee", obj.Table.Name);
        }

        [TestMethod]
        public void ShouldReadMultipleNodesAndProperties()
        {
            // Arrange
            var xml = @"<?xml version=""1.0""?>
                        <MyObject>
                            <Tables>
                                <Table>
                                    <Id>1</Id>
                                    <Name>eee</Name>
                                </Table>
                                <Table>
                                    <Id>2</Id>
                                    <Name>fff</Name>
                                </Table>
                            </Tables>
                        </MyObject>";

            // Act
            dynamic obj = DynamicXml.Parse(xml);

            // Assert
            Assert.AreEqual("1", obj.Tables[0].Id);
            Assert.AreEqual("eee", obj.Tables[0].Name);
            Assert.AreEqual("2", obj.Tables[1].Id);
            Assert.AreEqual("fff", obj.Tables[1].Name);
        }

        [TestMethod]
        public void ShouldReadMultipleNodesByInternalName()
        {
            // Arrange
            var xml = @"<?xml version=""1.0""?>
                        <MyObject>
                            <Tables>
                                <Table>
                                    <Id>1</Id>
                                    <Name>eee</Name>
                                </Table>
                                <Table>
                                    <Id>2</Id>
                                    <Name>fff</Name>
                                </Table>
                            </Tables>
                        </MyObject>";

            // Act
            dynamic obj = DynamicXml.Parse(xml);

            // Assert
            Assert.AreEqual("1", obj.Tables[0].Table.Id);
            Assert.AreEqual("eee", obj.Tables[0].Table.Name);
            Assert.AreEqual("2", obj.Tables[1].Table.Id);
            Assert.AreEqual("fff", obj.Tables[1].Table.Name);
        }

        [TestMethod]
        public void ShouldParseCustomData()
        {
            // Arrange
            var xml = @"<?xml version=""1.0""?>
                        <Configuration>
                          <Data>
                            <Response>
                              <params>
                                <param>
                                  <value>
                                    <struct>
                                      <member>
                                        <name>OKKNAME</name>
                                        <value>
                                          <string>OKKSTR</string>
                                        </value>
                                      </member>
                                      <member>
                                        <name>OKKNAME1</name>
                                        <value>
                                          <Obj>OKKOBJ</Obj>
                                        </value>
                                      </member>
                                      <member>
                                        <name>OKKLIST</name>
                                        <value>
                                          <array>
                                            <data>
                                              <value>
                                                <struct>
                                                  <member>
                                                    <name>OKKNAME3</name>
                                                    <value>
                                                      <string>OKKSTR</string>
                                                    </value>
                                                  </member>
                                                  <member>
                                                    <name>OKKNAME4</name>
                                                    <value>
                                                      <obj>OKKOBJ</obj>
                                                    </value>
                                                  </member>
                                                </struct>
                                              </value>
                                              <value>
                                                <struct>
                                                  <member>
                                                    <name>OKKNAME2</name>
                                                    <value>
                                                      <string/>
                                                    </value>
                                                  </member>
                                                </struct>
                                              </value>
                                            </data>
                                          </array>
                                        </value>
                                      </member>
                                    </struct>
                                  </value>
                                </param>
                              </params>
                            </Response>
                          </Data>
                        </Configuration>";

            // Act
            dynamic obj = DynamicXml.Parse(xml);

            Assert.IsNotNull(obj.Data.Response.Params[0]);
            Assert.IsNotNull(obj.Data.Response.Params[0].Value);
            Assert.IsNotNull(obj.Data.Response.Params[0].Value.Struct);
            Assert.IsNotNull(obj.Data.Response.Params[0].Value.Struct.Member[0]);
            Assert.IsNotNull(obj.Data.Response.Params[0].Value.Struct.Member[0].Value);
            Assert.AreEqual("OKKSTR", obj.Data.Response.Params[0].Value.Struct.Member[0].Value.@String);
        }

        [TestMethod]
        public void ShouldReadCData()
        {
            var xml = @" <Report creation=""20170607 18:47:09"">
						 	  <CustomData><![CDATA[<Src>OS0001</Src> <Dst>OS0002</Dst> <online>Online</online>
						 	]]></CustomData>
						 </Report> ";

            dynamic obj = DynamicXml.Parse(xml);

            var rootCData = DynamicXml.Parse($@"<root>{obj.CustomData}</root>").root;

            Assert.AreEqual("OS0001", rootCData.Src);
            Assert.AreEqual("OS0002", rootCData.Dst);
        }
    }
}